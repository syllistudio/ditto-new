<?php
class WPMetrics_Social_Widget extends WP_Widget {

    protected static $did_script = false;
    protected static $fields_count = 14;

    /**
     * Cunstruct
     */
    function __construct() {
        parent::__construct(
            'cms_social_widget', // Base ID
            esc_html__( 'CMS Social', 'wp-metrics' ), // Name
            array( 'description' => esc_html__( 'Social icons and links.', 'wp-metrics' ) ) // Args
        );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts') );
    }


    /**
     * Enqueue scripts and styles for the widget
     */
    function enqueue_scripts() {
        if ( ! self::$did_script && is_active_widget( false, false, $this->id_base, true ) ) {
            // wp_enqueue_script();
            self::$did_script = true;
        } 
    }


    /**
     * Outputs the HTML for this widget.
     *
     * @param   array   An array of standard parameters for widgets in this theme
     * @param   array   An array of settings for this widget instance
     * @return  void    Echoes it's output
     **/
    function widget( $args, $instance ) {
        extract( $args );

        if ( ! empty( $instance['title'] ) ) {
            $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
        }

        $alignment = empty( $instance['alignment'] ) ? 'text-inline' : $instance['alignment'];
        $spacing = empty( $instance['spacing'] ) ? 'spacing-default' : $instance['spacing'];
        $size = empty( $instance['size'] ) ? 'size-default' : $instance['size'];

        $list_classes = $alignment . ' ' . $spacing . ' ' . $size;

        echo wp_kses_post( $before_widget );

        if ( ! empty( $title ) )
            echo wp_kses_post( $before_title . $title . $after_title );

            echo '<ul class="cms-social ' . esc_attr( $list_classes ) . '">';
            foreach ( $instance['icons'] as $key => $icon ) {
                if ( ! empty( $icon['icon_class'] ) && ! empty( $icon['icon_link'] ) ) {
                    echo '<li class="' . esc_attr( $icon['icon_class'] ) . '">';
                    echo '<a href="' . esc_url( $icon['icon_link'] ) . '" target="_blank"><i class="fa fa-' . esc_attr( $icon['icon_class'] ) . '"></i></a>';
                    echo '</li>';
                }
            }
            echo '</ul>';

        echo wp_kses_post( $after_widget );
    }


    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param   array   An array of new settings as submitted by the admin
     * @param   array   An array of the previous settings
     * @return  array   The validated and (if necessary) amended settings
     **/
    function update( $new_instance, $old_instance ) {

        $instance = $old_instance;
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['alignment'] = strip_tags( $new_instance['alignment'] );
        $instance['spacing'] = strip_tags( $new_instance['spacing'] );
        $instance['size'] = strip_tags( $new_instance['size'] );
        $instance['icons'] = $new_instance['icons'];

        for ( $index = 0; $index < count( $instance['icons'] ); $index++ ) {
            $instance['icons'][$index]['icon_link'] = esc_url( $instance['icons'][$index]['icon_link'] );
            $instance['icons'][$index]['icon_class'] = esc_attr( $instance['icons'][$index]['icon_class'] );
        }

        return $instance;
    }


    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param   array   An array of the current settings for this widget
     * @return  void    Echoes it's output
     */
    function form( $instance ) {

        $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $alignment = isset( $instance['alignment'] ) ? esc_attr( $instance['alignment'] ) : '';
        $spacing = isset( $instance['spacing'] ) ? esc_attr( $instance['spacing'] ) : '';
        $size = isset( $instance['size'] ) ? esc_attr( $instance['size'] ) : '';
        ?>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'wp-metrics' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'alignment' ) ); ?>"><?php esc_html_e( 'Content Align:', 'wp-metrics' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'alignment' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'alignment' ) ); ?>">
                <option value="text-left"   <?php selected( $alignment, 'text-left' ); ?>><?php esc_html_e( 'Left', 'wp-metrics' ); ?></option>
                <option value="text-center" <?php selected( $alignment, 'text-center' ); ?>><?php esc_html_e( 'Center', 'wp-metrics' ); ?></option>
                <option value="text-right"  <?php selected( $alignment, 'text-right' ); ?>><?php esc_html_e( 'Right', 'wp-metrics' ); ?></option>
            </select>
        </p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'spacing' ) ); ?>"><?php esc_html_e( 'Spacing:', 'wp-metrics' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'spacing' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'spacing' ) ); ?>">
                <option value="spacing-default"   <?php selected( $spacing, 'spacing-default' ); ?>><?php esc_html_e( 'Default', 'wp-metrics' ); ?></option>
                <option value="spacing-wide" <?php selected( $spacing, 'spacing-wide' ); ?>><?php esc_html_e( 'Wide', 'wp-metrics' ); ?></option>
                <option value="spacing-wider"  <?php selected( $spacing, 'spacing-wider' ); ?>><?php esc_html_e( 'Wider', 'wp-metrics' ); ?></option>
            </select>
        </p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>"><?php esc_html_e( 'Size:', 'wp-metrics' ); ?></label>
            <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'size' ) ); ?>">
                <option value="size-default"   <?php selected( $size, 'size-default' ); ?>><?php esc_html_e( 'Default', 'wp-metrics' ); ?></option>
                <option value="size-medium" <?php selected( $size, 'size-medium' ); ?>><?php esc_html_e( 'Medium', 'wp-metrics' ); ?></option>
                <option value="size-big"  <?php selected( $size, 'size-big' ); ?>><?php esc_html_e( 'Big', 'wp-metrics' ); ?></option>
            </select>
        </p>
        <hr>

        <?php
        $icons = ! empty( $instance['icons'] ) ? $instance['icons'] : array();

        $field_counter = 0;

        while ( self::$fields_count > $field_counter ) {
            
            $icon_class = isset( $icons[$field_counter]['icon_class'] ) ? $icons[$field_counter]['icon_class'] : '';
            $icon_link = isset( $icons[$field_counter]['icon_link'] ) ? $icons[$field_counter]['icon_link'] : '';
            $label_icon_class = sprintf( '<strong>' . esc_html__( 'Icon %s:', 'wp-metrics' ) . '</strong>', $field_counter + 1 );
            $label_icon_link = sprintf( esc_html__( 'Icon %s link:', 'wp-metrics' ), $field_counter + 1 );

            echo sprintf(
                '<p><label for="%1$s">%2$s</label>' . $this->icon_choice_html(
                    esc_attr( $this->get_field_id( 'icons' ) . '_' . ( $field_counter ) . '_icon_class' ), // id
                    esc_attr( $this->get_field_name( 'icons' ) . '[' . ( $field_counter ) . '][icon_class]' ), // name
                    esc_attr( $icon_class ), // selected
                    'widefat'
                ),
                esc_attr( $this->get_field_id( 'icons' ) . '_' . ( $field_counter ) . '_icon_class' ), // label for
                $label_icon_class // label text
            );

            echo sprintf(
                '<p><label for="%1$s">%2$s</label><input class="widefat" id="%3$s" name="%4$s" type="text" value="%5$s" /></p>',
                esc_attr( $this->get_field_id( 'icons' ) . '_' . ( $field_counter ) . '_icon_link' ), // label for
                esc_attr( $label_icon_link ), // label text
                esc_attr( $this->get_field_id( 'icons' ) . '_' . ( $field_counter ) . '_icon_link' ), // id
                esc_attr( $this->get_field_name( 'icons' ) . '[' . ( $field_counter ) . '][icon_link]' ), // name
                esc_attr( $icon_link ) // value
            );

            echo '<hr>';

            $field_counter += 1;
        }
    }

    
    /**
     * Print out selection markup for icon choice
     */
    function icon_choice_html( $id, $name, $selected = '', $class = '' ) {
        ob_start(); ?>
        <select name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $class ); ?>">
            <option value=""><?php esc_html_e( '-- Select --', 'wp-metrics' ); ?></option>
            <option value="behance" <?php selected( $selected, "behance" );?>><?php esc_html_e( 'Behance', 'wp-metrics' ); ?></option>
            <option value="dribbble" <?php selected( $selected, "dribbble" );?>><?php esc_html_e( 'Dribbble', 'wp-metrics' ); ?></option>
            <option value="facebook" <?php selected( $selected, "facebook" );?>><?php esc_html_e( 'Facebook', 'wp-metrics' ); ?></option>
            <option value="flickr" <?php selected( $selected, "flickr" );?>><?php esc_html_e( 'Flickr', 'wp-metrics' ); ?></option>
            <option value="github" <?php selected( $selected, "github" );?>><?php esc_html_e( 'Github', 'wp-metrics' ); ?></option>
            <option value="google" <?php selected( $selected, "google" );?>><?php esc_html_e( 'Google', 'wp-metrics' ); ?></option>
            <option value="instagram" <?php selected( $selected, "instagram" );?>><?php esc_html_e( 'Instagram', 'wp-metrics' ); ?></option>
            <option value="linkedin" <?php selected( $selected, "linkedin" );?>><?php esc_html_e( 'LinkedIn', 'wp-metrics' ); ?></option>
            <option value="pinterest" <?php selected( $selected, "pinterest" );?>><?php esc_html_e( 'Pinterest', 'wp-metrics' ); ?></option>
            <option value="rss" <?php selected( $selected, "rss" );?>><?php esc_html_e( 'Rss', 'wp-metrics' ); ?></option>
            <option value="skype" <?php selected( $selected, "skype" );?>><?php esc_html_e( 'Skype', 'wp-metrics' ); ?></option>
            <option value="tumblr" <?php selected( $selected, "tumblr" );?>><?php esc_html_e( 'Tumblr', 'wp-metrics' ); ?></option>
            <option value="twitter" <?php selected( $selected, "twitter" );?>><?php esc_html_e( 'Twitter', 'wp-metrics' ); ?></option>
            <option value="vimeo" <?php selected( $selected, "vimeo" );?>><?php esc_html_e( 'Vimeo', 'wp-metrics' ); ?></option>
            <option value="yahoo" <?php selected( $selected, "yahoo" );?>><?php esc_html_e( 'Yahoo', 'wp-metrics' ); ?></option>
        </select>
        <?php
        return ob_get_clean();
    }

}

add_action( 'widgets_init', create_function( '', "register_widget( 'WPMetrics_Social_Widget' );" ) );