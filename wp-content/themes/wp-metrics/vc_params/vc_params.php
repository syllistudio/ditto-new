<?php defined( 'ABSPATH' ) or exit();
/**
 * Additional params for Visual Composer Components
 *
 * @package CMSSuperHeroes
 * @subpackage WPMetrics
 */

$theme_fonts = array();
$font = wpmetrics_get_theme_option( 'font_alt_1', false );
if ( isset( $font ) && ! empty( $font ) ) {
    if ( isset( $font['font-family'] ) && ! empty( $font['font-family'] ) ) {
        $theme_fonts[ strval( $font['font-family'] ) ] = 'font-alt-1';
    }
}

$font = null; $font = wpmetrics_get_theme_option( 'font_alt_2', false );
if ( isset( $font ) && ! empty( $font ) ) {
    if ( isset( $font['font-family'] ) && ! empty( $font['font-family'] ) ) {
        $theme_fonts[ strval( $font['font-family'] ) ] = 'font-alt-2';
    }
}

$font = wpmetrics_get_theme_option( 'font_alt_3', false );
if ( isset( $font ) && ! empty( $font ) ) {
    if ( isset( $font['font-family'] ) && ! empty( $font['font-family'] ) ) {
        $theme_fonts[ strval( $font['font-family'] ) ] = 'font-alt-3';
    }
}

vc_add_param( 'vc_row', array(
    'type' => 'checkbox',
    'param_name' => 'overlay',
    'heading' => esc_html__( 'Enable Overlay', 'wp-metrics' ),
    'description' => esc_html__( 'Set background color / image and the color will show up above image. Recomended Color with alpha lower than 100%', 'wp-metrics' )
) );


vc_add_param( 'vc_row_inner', array(
    'type' => 'checkbox',
    'param_name' => 'overlay',
    'heading' => esc_html__( 'Enable Overlay', 'wp-metrics' ),
    'description' => esc_html__( 'Set background color / image and the color will show up above image. Recomended Color with alpha lower than 100%', 'wp-metrics' )
) );

vc_add_param( 'vc_column', array(
    'type' => 'checkbox',
    'param_name' => 'fill_place',
    'heading' => esc_html__( 'Fill remaining space', 'wp-metrics' ),
    'description' => esc_html__( 'Set parent row to stretch, and this option will make your column to fill all remaining space.', 'wp-metrics' )
) );


vc_add_param( 'vc_column_inner', array(
    'type' => 'checkbox',
    'param_name' => 'fill_place',
    'heading' => esc_html__( 'Fill remaining space', 'wp-metrics' ),
    'description' => esc_html__( 'Set parent row to stretch, and this option will make your column to fill all remaining space.', 'wp-metrics' )
) );

vc_add_param( 'vc_column_text', array(
    'type' => 'dropdown',
    'param_name' => 'theme_font',
    'heading' => esc_html__( 'Built-in theme fonts', 'wp-metrics' ),
    'description' => esc_html__( 'Select font for the text, you can set these fonts at Appearance/Theme Options', 'wp-metrics' ),
    'value' => array_merge(
        array(
            esc_html__( 'Default', 'wp-metrics' ) => ''
        ),
        $theme_fonts
    ),
    'std' => 'font-alt-3'
) );
vc_add_param( 'vc_column_text', array(
    'type' => 'font_container',
    'param_name' => 'font_container',
    'settings' => array(
        'fields' => array(
            'font_size',
            'line_height',
            'font_size_description' => esc_html__( 'Enter font size.', 'wp-metrics' ),
            'line_height_description' => esc_html__( 'Enter line height.', 'wp-metrics' ),
        ),
    )
) );

vc_add_param( 'vc_custom_heading', array(
    'type' => 'textfield',
    'param_name' => 'letter_spacing',
    'heading' => esc_html__( 'Letter spacing', 'wp-metrics' ),
    'description' => esc_html__( 'Enter letter spacing', 'wp-metrics' )
) );
vc_add_param( 'vc_custom_heading', vc_map_add_css_animation( true ) );

vc_remove_param( 'vc_tta_accordion', 'style' );
vc_remove_param( 'vc_tta_accordion', 'shape' );
vc_remove_param( 'vc_tta_accordion', 'color' );
vc_remove_param( 'vc_tta_accordion', 'spacing' );
vc_add_param( 'vc_tta_accordion', array(
    'heading' => esc_html__( 'Gap', 'wp-metrics' ),
    'type' => 'dropdown',
    'param_name' => 'gap',
    'value' => array(
        esc_html__( 'Theme Default', 'wp-metrics' ) => 'default',
        esc_html__( 'None', 'wp-metrics' ) => '',
        esc_html__( '1px', 'wp-metrics' ) => '1',
        esc_html__( '2px', 'wp-metrics' ) => '2',
        esc_html__( '3px', 'wp-metrics' ) => '3',
        esc_html__( '4px', 'wp-metrics' ) => '4',
        esc_html__( '5px', 'wp-metrics' ) => '5',
        esc_html__( '10px', 'wp-metrics' ) => '10',
        esc_html__( '15px', 'wp-metrics' ) => '15',
        esc_html__( '20px', 'wp-metrics' ) => '20',
        esc_html__( '25px', 'wp-metrics' ) => '25',
        esc_html__( '30px', 'wp-metrics' ) => '30',
        esc_html__( '35px', 'wp-metrics' ) => '35'
    )
) );

vc_remove_param( 'vc_tta_tabs', 'style' );
vc_remove_param( 'vc_tta_tabs', 'shape' );
vc_remove_param( 'vc_tta_tabs', 'color' );
vc_remove_param( 'vc_tta_tabs', 'spacing' );

vc_remove_param( 'vc_tta_tour', 'style' );
vc_remove_param( 'vc_tta_tour', 'shape' );
vc_remove_param( 'vc_tta_tour', 'color' );
vc_remove_param( 'vc_tta_tour', 'spacing' );


/**
 * Parses google_fonts_data and font_container_data to get needed css styles to markup
 *
 * @param $el_class
 * @param $css
 * @param $google_fonts_data
 * @param $font_container_data
 * @param $atts
 *
 * @since VC4.3
 * @return array
 */
function wpmetrics_vc_get_font_container_styles( $el_class, $css, $google_fonts_data, $font_container_data, $atts ) {
    $styles = array();
    if ( ! empty( $font_container_data ) && isset( $font_container_data['values'] ) ) {
        foreach ( $font_container_data['values'] as $key => $value ) {
            if ( 'tag' !== $key && strlen( $value ) ) {
                if ( preg_match( '/description/', $key ) ) {
                    continue;
                }
                if ( 'font_size' === $key || 'line_height' === $key ) {
                    $value = preg_replace( '/\s+/', '', $value );
                }
                if ( 'font_size' === $key ) {
                    $pattern = '/^(\d*(?:\.\d+)?)\s*(px|\%|in|cm|mm|em|rem|ex|pt|pc|vw|vh|vmin|vmax)?$/';
                    // allowed metrics: http://www.w3schools.com/cssref/css_units.asp
                    $regexr = preg_match( $pattern, $value, $matches );
                    $value = isset( $matches[1] ) ? (float) $matches[1] : (float) $value;
                    $unit = isset( $matches[2] ) ? $matches[2] : 'px';
                    $value = $value . $unit;
                }
                if ( strlen( $value ) > 0 ) {
                    $styles[] = str_replace( '_', '-', $key ) . ': ' . $value;
                }
            }
        }
    }
    if ( ( ! isset( $atts['use_theme_fonts'] ) || 'yes' !== $atts['use_theme_fonts'] ) && ! empty( $google_fonts_data ) && isset( $google_fonts_data['values'], $google_fonts_data['values']['font_family'], $google_fonts_data['values']['font_style'] ) ) {
        $google_fonts_family = explode( ':', $google_fonts_data['values']['font_family'] );
        $styles[] = 'font-family:' . $google_fonts_family[0];
        $google_fonts_styles = explode( ':', $google_fonts_data['values']['font_style'] );
        $styles[] = 'font-weight:' . $google_fonts_styles[1];
        $styles[] = 'font-style:' . $google_fonts_styles[2];
    }

    /**
     * Filter 'VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG' to change vc_custom_heading class
     *
     * @param string - filter_name
     * @param string - element_class
     * @param string - shortcode_name
     * @param array - shortcode_attributes
     *
     * @since 4.3
     */
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_custom_heading ' . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );

    return array(
        'css_class' => trim( preg_replace( '/\s+/', ' ', $css_class ) ),
        'styles' => $styles,
    );
}