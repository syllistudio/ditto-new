<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 * 
 * @package WPMetrics
 */
get_template_part( 'template-parts/helper', 'footer' );
?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
