<?php

function wp_metrics_child_add_stylesheet() {

	wp_enqueue_style('parent-style', get_template_directory_uri() .'/style.css');

	wp_enqueue_style('js_composer_front');
	wp_enqueue_style( 'custom-theme', get_stylesheet_directory_uri() . '/style.css', array('parent-style'));
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/font-awesome/css/font-awesome.css');

}
add_action( 'wp_enqueue_scripts', 'wp_metrics_child_add_stylesheet');

function my_scripts_method() {
    wp_enqueue_script(
        'bhu-datepicker',
        get_stylesheet_directory_uri() . '/js/jquery.ui.datepicker.ext.be.js',
        array('jquery','jquery-ui-datepicker')
    );

    wp_enqueue_script(
        'ellipsis-js', get_stylesheet_directory_uri() . '/js/jquery.ellipsis.js', array('jquery')
    );

    wp_enqueue_script(
        'custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery')
    );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

# HOOK: for custom header background image at single product
add_filter( 'get_post_metadata', '_custom_page_title', 10, 4 );
function _custom_page_title( $metadata, $object_id, $meta_key, $single ) {
	if ( isset( $meta_key ) && $meta_key == '_cms_custom_page_title' && is_singular( 'product' ) ) {
		return 'hidden';
	} 
}

# HOOK: set product single image use original size('full')
add_filter( 'single_product_large_thumbnail_size', '_single_product_large_thumb', 10, 1 );
function _single_product_large_thumb( $type ) {
	if ( $type == 'shop_single' && is_singular( 'product' ) ) {
		return 'full';
	}
}

# HOOK: cutomise dropdown filter by categories at shop page
add_action( 'ditto_woocommerce_before_shop_loop', 'ditto_woocommerce_catalog_filtering', 10 );
if ( ! function_exists( 'ditto_woocommerce_catalog_filtering' ) ) { 
	/**
	 * Output the product filtering options.
	 *
	 * @subpackage	Loop
	 */
	function ditto_woocommerce_catalog_filtering() {
		global $wp_query;

		if ( 1 === $wp_query->found_posts || ! woocommerce_products_will_display() ) {
			return;
		}

		$filterby = isset( $_GET['filterby'] ) ? wc_clean( $_GET['filterby'] ) : apply_filters( 'woocommerce_default_catalog_filterby', get_option( 'woocommerce_default_catalog_filterby' ) );

		$catalog_options = array();

		$terms = get_terms( array(
		    'taxonomy' => 'product_cat',
		    'hide_empty' => false,
		    'orderby' => 'name',
		    'order' => 'asc',
		) );

		foreach ($terms as $term) {
			$catalog_options[$term->slug] = $term->name;
		}
		

		wc_get_template( 'loop/filterby.php', array( 'catalog_options' => $catalog_options, 'filterby' => $filterby, 'show_default_orderby' => true ) );
	}
}

function ditto_dropdown_filter_by_categories( $catalog_options, $filterby ) {
    ob_start();
    ?><select name="filterby" class="filterby" onchange="this.form.submit()">
    	<option value=""><?= _e('All categories'); ?></option>
        <?php foreach ( $catalog_options as $id => $name ) : ?>
            <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $filterby, $id ); ?>><?php echo esc_html( $name ); ?></option>
        <?php endforeach; ?>
    </select>
    <?php
    // Keep query string vars intact
    foreach ( $_GET as $key => $val ) {
        if ( 'filterby' === $key || 'submit' === $key || 'per_page' === $key || 'view' === $key ) {
            continue;
        }
        if ( is_array( $val ) ) {
            foreach( $val as $innerVal ) {
                echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
            }
        } else {
            echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
        }
    }

    echo ob_get_clean();
}

add_action( 'pre_get_posts', 'ditto_filter_by_categories', 2 );
function ditto_filter_by_categories($q) {
	// We only want to affect the main query
	if ( ! $q->is_main_query() ) {
		return;
	}

	if ( isset($_GET['filterby']) ) {
		if ( ! empty($_GET['filterby']) ) {
			$taxquery = array(
				array(
					'taxonomy' => 'product_cat',
					'field' => 'slug',
					'terms' => sanitize_text_field($_GET['filterby']),
				)
			);

		    $q->set( 'tax_query', $taxquery );
		}
	}
}


// custom shortcode

add_action( 'vc_before_init', 'hover_section_integrateWithVC' );
function hover_section_integrateWithVC() {
   vc_map( array(
      "name" => __( "Ditto Hover Element", "my-text-domain" ),
      "base" => "ditto_hover_element",
      "class" => "",
      "category" => __( "Content", "my-text-domain"),
      "params" => array(
      	array(
            "type" => "attach_image",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Image", "my-text-domain" ),
            "param_name" => "img",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "", "my-text-domain" )
         ),
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Title", "my-text-domain" ),
            "param_name" => "title",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "", "my-text-domain" )
         ),
         array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Content", "my-text-domain" ),
            "param_name" => "content",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "", "my-text-domain" )
         ),
         array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __( "Hover color", "my-text-domain" ),
            "param_name" => "color",
            "value" => '#293895',
            "description" => __( "Choose color for background after hover this box.", "my-text-domain" )
         ),
         array(
            "type" => "vc_link",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Link Url", "my-text-domain" ),
            "param_name" => "link",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "", "my-text-domain" )
         ),
      )
   ) );
}

add_shortcode( 'ditto_hover_element', 'ditto_hover_element_func' );
function ditto_hover_element_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'img' => '',
	  'title' => '',
      'color' => '#293895',
      'link' => ''
   ), $atts ) );

   $html = '';

   ob_start();
   ?>

   <?php
   		if(!empty($link)){
	   		$link = vc_build_link($link);
	   	}
	   	if(empty($link['url']))
   ?>
  <?php

        wp_enqueue_script( 'waypoints' );
        $output = ' wpb_animate_when_almost_visible wpb_appear';
  ?>
   <div class="ditto_hover_element_container wpb_content_element <?=$output?> ">
   		<div class="image">
   			<a href="<?php if(empty($link['url'])){ echo "#"; }else{ echo $link['url']; } ?>" class="link_title">
	   			<?php
	   				if(!empty($img)){
		   				$image_url = wp_get_attachment_image_src($img , array('220','220'), true );
		   			}
	   			?>
	   			<img src="<?=$image_url[0]?>" />
	   			<div class="hover_box" style="background-color: <?=$color?>;"></div>
	   			<h3 class="inner_hover_box_title"><?=$title?></h3>
   			</a>
   		</div>
   		<div class="title">
   			<h3 class="main_title"><?=$title?></h3>
   			<div class="content_box"">
   				<p><?= $content ?></p>
   			</div>
   		</div>
   </div>
   <?php
     $html = ob_get_contents();

     ob_end_clean();
  
   return $html;
}