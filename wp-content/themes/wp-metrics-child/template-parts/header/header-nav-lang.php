<?php defined( 'ABSPATH' ) or exit();

$languages = icl_get_languages('skip_missing=0');
$en = $languages['en'];
$th = $languages['th'];
$actth = ($th['active'] ? 'actived-lang': '');
$acten = ($en['active'] ? 'actived-lang': '');

echo '<ul class="cms-menu-lang">';
echo    '<li class="'. $actth .'"><a class="lang-toggle" href="'. $th['url'] .'" title="' . esc_attr__( 'Thai', 'wp-metrics' ) . '">TH</a></li>';
echo    '<li><span>/</span></li>';
echo  	'<li class="'. $acten .'"><a class="lang-toggle" href="'. $en['url'] .'" title="' . esc_attr__( 'English', 'wp-metrics' ) . '">EN</a></li>';
echo  '</ul>';