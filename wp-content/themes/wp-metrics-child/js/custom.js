jQuery(document).ready(function($){
	

	$('.blog .entry-content, .archive.category .entry-content, .archive.date .entry-content').ellipsis({
	    row: 4
	});

	$('.post-summary h4').ellipsis({
	    row: 1
	});


	if($('select[name=applied_job_position]').length > 0){
		$('select[name=applied_job_position]').on('change',function(){
			var val = $(this).val();

			$('.other_job').hide();

			if(val == 'อื่นๆ'){
				$('.other_job').show();
			}else{
				$('.other_job').find('input').val('');
			}
		});
	}

});