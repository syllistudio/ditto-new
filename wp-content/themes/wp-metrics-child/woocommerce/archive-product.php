<?php
get_header();
?>
<div id="page" class="site">


    <?php //get_template_part( 'template-parts/helper', 'product-archive' ); ?>
    <?php
        /**
         * woocommerce_before_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         */
        do_action( 'woocommerce_before_main_content' );
    ?>

    <?php
        /**
         * woocommerce_archive_description hook.
         *
         * @hooked woocommerce_taxonomy_archive_description - 10
         * @hooked woocommerce_product_archive_description - 10
         */
        do_action( 'woocommerce_archive_description' );
    ?>

    <?php
        /**
         * woocommerce_before_shop_loop hook.
         *
         * @hooked ditto_woocommerce_result_count - 20
         * @hooked ditto_woocommerce_catalog_filtering - 30
         */
        do_action( 'ditto_woocommerce_before_shop_loop' );

    ?>

    <?php if ( have_posts() ) : ?>

        <?php woocommerce_product_loop_start(); ?>

            <?php woocommerce_product_subcategories(); ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <?php wc_get_template_part( 'content', 'product' ); ?>

            <?php endwhile; // end of the loop. ?>

        <?php woocommerce_product_loop_end(); ?>

        <?php
            /**
             * woocommerce_after_shop_loop hook.
             *
             * @hooked woocommerce_pagination - 10
             */
            do_action( 'woocommerce_after_shop_loop' );
        ?>

    <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

        <?php wc_get_template( 'loop/no-products-found.php' ); ?>

    <?php endif; ?>

    <?php
        /**
         * woocommerce_after_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
        do_action( 'woocommerce_after_main_content' );
    ?>
<?php get_footer(); ?>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.current-menu-item').closest('.menu-item.menu-item-has-children').addClass('current-menu-item');
        $('#page_title').hide();
    });
</script>