<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see         http://docs.woothemes.com/document/template-structure/
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$options = isset( $view_per_page_options['options'] ) && is_array( $view_per_page_options['options'] ) ? $view_per_page_options['options'] : array();
$total = isset( $view_per_page_options['total'] ) ? $view_per_page_options['total'] : 1;
$paged = isset( $view_per_page_options['paged'] ) ? $view_per_page_options['paged'] : 1;
$current_value = isset( $view_per_page_options['current'] ) ? $view_per_page_options['current'] : 12;
$current_view = isset( $_GET['view'] ) ? esc_html( $_GET['view'] ) : 'grid';
?>
<div class="shop-main-filter shop-front-filter-wrapper">
    <form class="woocommerce-ordering woocommerce-filterby" action="http://www.dittothailand.com/ditto-products-service/hardware/" method="get" style="margin-top:30px;">
        <div class="shop-main-filter-right">
            <label for="filterby"><?php _e('Show'); ?> :   </label>
            <?php ditto_dropdown_filter_by_categories( $catalog_options, $filterby ); ?>
        </div>
    </form>
</div>

