<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @package WPMetrics
 */

get_header();
?>
<div id="content" class="site-content">
    <div class="container">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <?php
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>
            </main><!-- #main -->
        </div><!-- #primary -->
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($){

    
	$('input[name=applicant_age]').attr('readonly','readonly');
	$(document).on('change', 'input[name=applicant_birthdate]', function(){
		var ele = $(this);
		dob = new Date(ele.val());

		var today = new Date();

		var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));

        age = age+543;

		$('input[name=applicant_age]').val(age);

	});

	$(document).on('click', 'input[name=applicant_birthdate]', function(){
		$('input[name=applicant_birthdate]').trigger('change');

	});

    $( "input[name=applicant_birthdate]").datepicker({ isBE: true, autoConversionField: false }); 
});
</script>
<?php
get_footer();
